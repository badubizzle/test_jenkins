defmodule TestJenkins do
  @moduledoc """
  Documentation for TestJenkins.
  """

  @doc """
  Hello world.

  ## Examples

      iex> TestJenkins.hello()
      :world_world_2

  """
  def hello do
    :world_world_2
  end
end
